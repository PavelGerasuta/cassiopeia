const calcuateButton = document.getElementById('calculate-button');
const calculator = document.getElementById('calculator');


calcuateButton.onclick = function () {
  let reOne = Number(document.getElementById('re-one').value);
  let imOne = Number(document.getElementById('im-one').value);
  let reTwo = Number(document.getElementById('re-two').value);
  let imTwo = Number(document.getElementById('im-two').value);

  if ((isNaN(reOne) == false) && (isNaN(imOne) == false) && (isNaN(reTwo) == false) && (isNaN(imTwo) == false) ) {
    let ComplexOne = new Complex(reOne, imOne);
    let ComplexTwo = new Complex(reTwo, imTwo);
  
    let operation = document.getElementById('operation').value
  
    switch (operation) {
      case 'summarize':
        render(summarize(ComplexOne, ComplexTwo));
        break
      case 'subtraction':
        render(subtraction(ComplexOne, ComplexTwo));
        break
      case 'multiplication':
        render(multiplication(ComplexOne, ComplexTwo));
        break
      case 'division':
        division(ComplexOne, ComplexTwo);
        break
    }
  
  } else {
     alert("Не правильный формат данных");
  }

}

function Complex(Re, Im) {
  this.Re = Re;
  this.Im = Im;
};

function render(answer) {
  reAnswer = document.getElementById('re-answer');
  imAnswer = document.getElementById('im-answer');
  
  reAnswer.value = answer.Re;

  if (answer.Im > 0) {
    imAnswer.value = "+"  + answer.Im + "i";
  } else {
    imAnswer.value = answer.Im + "i";
  }

  document.getElementById('summ-sub-mul-answer').style.visibility = "visible";

  document.getElementById('division-answer').style.visibility = "hidden";
}

function  summarize(z1, z2) {
  let re = z1.Re + z2.Re;
  let im = z1.Im + z2.Im;

  let z3 = new Complex(re, im);
  return z3;
}

function subtraction(z1, z2) {
  let re = z1.Re - z2.Re;
  let im = z1.Im - z2.Im;

  let z3 = new Complex(re, im);
  return z3;
}

function multiplication(z1, z2) {
  let re1 = z1.Re*z2.Re;
  let im1 = z1.Re*z2.Im;
  let im2 = z1.Im*z2.Re;
  let re2 = (z1.Im*z2.Im) * (-1);

  let re = re1 + re2;
  let im = im1 + im2;

  let z3 = new Complex(re, im);

  return z3;
}

function division(z1, z2) {
  let complex_conjugate = new Complex(z2.Re, -z2.Im);
  let numerator = multiplication(z1, complex_conjugate);
  let denominator = multiplication(z2, complex_conjugate);

  document.getElementById('summ-sub-mul-answer').style.visibility = "hidden";

  document.getElementById('division-answer').style.visibility = "visible";

  reNumerator = document.getElementById('numerator-re');
  imNumerator = document.getElementById('numerator-im');

  reDenominator = document.getElementById('denominator-re');
  imDenominator = document.getElementById('denominator-im');

  if ( numerator.Re != 0) { 
    reNumerator.value = numerator.Re;
  }
  

  if ( numerator.Im != 0) {
    if (numerator.Im > 0) {
      imNumerator.value = " + "  + numerator.Im  + "i";
    } else {
      imNumerator.value = numerator.Im  + "i";
    }
  }

  if ( denominator.Re != 0) { 
    reDenominator.value = denominator.Re;
  }

  if ( denominator.Im != 0) {
    if (denominator.Im > 0) {
      imDenominator.value = " + "  + denominator.Im  + "i";
    } else {
      imDenominator.value = denominator.Im  + "i";
    } 
  }
}
